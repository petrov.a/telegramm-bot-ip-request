# Telegram Bot IP request



## Подготовка

1) Установите следующие необходимые библиотеки в Командной строке.
```
py -m pip install requests
py -m pip install pytelegrambotapi -U
py -m pip install DateTime
py -m pip install markdown
```

## Компиляция в .exe файл

```
C:\Users\%USERPROFILE%\AppData\Local\Programs\Python\Python311\Scripts\pyinstaller -F -w %USERPROFILE%\Desktop\telegram_ip_bot\ip_bot.py
```

P.S.

> Запрос ID пользователя:
`https://t.me/myidbot -> /getid`

> Создание бота:
`https://t.me/BotFather -> /newbot`
