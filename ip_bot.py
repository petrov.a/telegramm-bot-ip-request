import requests
import telebot
import config
import http.client
import datetime
import markdown
from telebot import types
from datetime import datetime 


### Функция проверки авторизации
def autor(chatid):
    strid = str(chatid)
    for item in config.users:
        if item == strid:
            return True
    return False
    
### Token telegram bot
client = telebot.TeleBot("") 


########################
@client.message_handler(commands=['start'])
def yourip(message):
   markup_reply = types.ReplyKeyboardMarkup(resize_keyboard = True)
   item_yes = types.InlineKeyboardButton(text = 'Запросить IP', callback_data = 'yes')

   markup_reply.add(item_yes)
   client.send_message(message.chat.id, f'Добрый день, {message.from_user.first_name} {message.from_user.last_name}.\nЖелаете узнать свой IP ?',
   reply_markup = markup_reply
   )
@client.callback_query_handler(func = lambda call: True)
  

@client.message_handler(content_types = ['text'])
def get_text (message):
   if message.text == 'Запросить IP':
        conn = http.client.HTTPConnection("ifconfig.me")
        conn.request("GET", "/ip")
        ip = conn.getresponse().read()  

        client.send_message(message.chat.id, ip,)
        client.send_message(message.chat.id, f'_IP-адрес запрошен\n{datetime.now().strftime("%d.%m.%Y в %H:%M")}_', parse_mode='Markdown')

        
client.polling(none_stop = True, interval = 0)
